<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if ($this->image) {
            $imageUrl = asset('storage/'.$this->image);
        }else{
            $imageUrl = asset('storage/no_image.png');
        }

        return [
            'id' => $this->id,
            'title' => $this->title,
            'category_id' => $this->category_id,
            'description' => $this->description,
            'image' => $this->image,
            'image_url' => $imageUrl
        ];
    }
}
