<?php

namespace App\Http\Controllers;

use App\Product;
use App\Http\Requests\ProductRequests;
use App\Http\Resources\ProductResource;
use Illuminate\Http\Request;


class ProductsController extends Controller
{

    /**
     * @param Request $requests
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $requests)
    {
        $product = new Product();
        if($requests->sort and sizeof($requests->sort) >= 2) {
            $product = $product->orderBy($requests->sort[1],$requests->sort[0]);
        }

        if($requests->filter and isset($requests->filter['category_id'])) {
            $product = $product->where('category_id', $requests->filter['category_id']);
        }

        // usleep(1000000);

        return ProductResource::collection($product->get());
    }

    /**
     * @param Category $product
     * @return ProductResource
     */
    public function create(Request $requests, Product $product)
    {
        if ($requests->category_id) {
            $product->category_id = $requests->category_id;
        }else{
            $product->category_id = 0;
        }

        return new ProductResource($product);
    }

    /**
     * @param ProductRequests $requests
     * @param Product $product
     * @return ProductResource
     */
    public function store(ProductRequests $requests)
    {
        if ($requests->validator and $requests->validator->fails()) {
            return $requests->validator->fails()->toArray();
        }

        $product = Product::create($requests->validated());

        return new ProductResource($product);
    }

    /**
     * @param Product $product
     * @return ProductResource
     */
    public function show(Product $product)
    {
        return new ProductResource($product);
    }

    /**
     * @param Product $product
     * @return ProductResource
     */
    public function edit(Product $product)
    {
        return new ProductResource($product);
    }

    /**
     * @param ProductRequests $requests
     * @param Product $product
     * @return ProductResource
     */
    public function update(ProductRequests $requests, Product $product)
    {
        if ($requests->validator and $requests->validator->fails()) {
            return $requests->validator->fails()->toArray();
        }

        $product->fill($requests->validated());
        $product->save();
        return new ProductResource($product);
    }

    /**
     * @param Product $product
     * @return array
     * @throws \Exception
     */
    public function destroy(Product $product)
    {
        $product->delete();
        return [];
    }
}
