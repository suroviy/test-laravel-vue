<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\CategoryRequests;
use App\Http\Resources\CategoryResource;

class CategoriesController extends Controller
{

    /**
     * @param Category $category
     * @return \App\Http\Resources\CategoryResource
     */
    public function index(Category $category)
    {
        return CategoryResource::collection($category->defaultOrder()->get());
    }

    /**
     * @param Category $category
     * @return array
     */
    public function create(Category $category)
    {
        return new CategoryResource($category);
    }

    /**
     * @param CategoryRequests $requests
     * @param Category $category
     * @return array
     */
    public function store(CategoryRequests $requests)
    {
        if ($requests->validator and $requests->validator->fails()) {
            return $requests->validator->fails()->toArray();
        }
        $category = Category::create($requests->validated());
        return new CategoryResource($category);
    }

    /**
     * @param CategoryRequests $requests
     * @param Category $category
     * @return array
     */
    public function show(Category $category)
    {
        return new CategoryResource($category);
    }

    /**
     * @param CategoryRequests $requests
     * @param Category $category
     * @return array
     */
    public function edit(Category $category)
    {
        return new CategoryResource($category);
    }

    /**
     * @param CategoryRequests $requests
     * @param Category $category
     * @return string
     */
    public function update(CategoryRequests $requests, Category $category)
    {
        if ($requests->validator and $requests->validator->fails()) {
            return $requests->validator->fails()->toArray();
        }

        $category->fill($requests->validated());

        if($requests->move == 'up') $category->up();
        if($requests->move == 'down') $category->down();

        $category->save();
        return new CategoryResource($category);
    }

    /**
     * @param Category $category
     * @return array
     * @throws \Exception
     */
    public function destroy(Category $category)
    {
        $category->delete();
        return [];
    }
}
