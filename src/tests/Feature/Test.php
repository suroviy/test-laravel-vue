<?php

namespace Tests\Feature;

use Tests\AbstractTestCase;
use Illuminate\Http\UploadedFile;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Storage;

class Test extends AbstractTestCase
{
    /**
     * @return void
     */
    public function testCategoryController()
    {

        $response = $this->get('/api/categories');
        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                0 => [
                    'id' => 1,
                    'name' => 'category 1'
                ]
            ],
        ]);

        $response = $this->get('/api/categories/1');
        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'id' => 1,
                'name' => 'category 1'
            ],
        ]);

        $response = $this->json('PUT', '/api/categories/1', ['name' => 'category rename','move' => "down"]);
        $response->assertStatus(200);

        $response = $this->get('/api/categories');
        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                1 => [
                    'id' => 1,
                    'name' => 'category rename'
                ]
            ],
        ]);

        $response = $this->get('/api/categories/1');
        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'id' => 1,
                'name' => 'category rename'
            ],
        ]);

        $response = $this->get('/api/categories/10');
        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'id' => 10,
                'name' => 'category 10'
            ],
        ]);

        $response = $this->delete('/api/categories/10');
        $response->assertStatus(200);

        $response = $this->get('/api/categories/10');
        $response->assertStatus(404);

        $this->assertTrue(true);
    }

    public function testProductController()
    {
        $response = $this->get('/api/products',['filter'=>['category_id'=>1]]);

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                0 => [
                    'id' => 1,
                    'category_id' => 1
                ]
            ],
        ]);

        $response = $this->get('/api/products/1');
        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'id' => 1,
                'category_id' => 1
            ],
        ]);

        $response = $this->json('PUT', '/api/products/1', ['title' => 'product rename','description' => "new description", 'category_id' =>2, 'image' => 'no_image.png']);
        $response->assertStatus(200);

        $response = $this->get('/api/products/1');
        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'id' => 1,
                'category_id' => 2,
                'title' => 'product rename',
                'description' => 'new description'
            ]
        ]);

        $response = $this->delete('/api/products/54');
        $response->assertStatus(200);

        $response = $this->get('/api/products/54');
        $response->assertStatus(404);

        $this->assertTrue(true);
    }

    public function testUploadController() {
        $file = UploadedFile::fake()->image('test.png');

        $response = $this->json('POST', '/api/upload', [
            'image' => $file,
        ]);
        $response->assertStatus(200);
        $response->assertJson([
            'name' => true
        ]);

        $nameImage = $response->json('name');
        Storage::disk('public')->assertExists($nameImage);

        $this->assertTrue(true);
    }
}
