<?php

use Faker\Generator as Faker;

/** @var EloquentFactory $factory */
$factory->define(App\Product::class, function (Faker $faker) {
    return [
        'title' => $faker->title,
        'image' => $faker->image,
    ];
});
