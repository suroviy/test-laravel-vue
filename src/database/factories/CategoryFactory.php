<?php

use Faker\Generator as Faker;

/** @var EloquentFactory $factory */
$factory->define(App\Category::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
    ];
});
