<?php

use App\Category;
use App\Product;
use Illuminate\Database\Seeder;

class CategoriesProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=0;$i<10;$i++) {
            factory(Category::class)->create([
                'name' => 'category '.($i+1)
            ])->each(function($cat) {
                $cat->products()->save($this->createProduct(1, $cat));
            });
        }
    }

    private function createProduct($i, $cat) {
        return factory(Product::class)->create([
                'title' => ' Продукт категории ' . $cat->id,
                'description' => 'Описание очень подробное описание, в мелких деталях описание присутствует здесь',
                'image' => 'no_image.png',
            ]);
    }
}
