<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('categories', 'CategoriesController');
Route::resource('products', 'ProductsController');

Route::post('upload', function (Request $request){
    if ($request->hasFile('image') and $request->file('image')->isValid())
    {
        $nameFile = uniqid().'.'.$request->image->extension();
        $request->image->storeAs('public', $nameFile);
        return response()->json(['name' => $nameFile]);
    }
    return response(500)->json(['error'=> ['image' =>'not valid']]);
});
