import Vue        from 'vue'
import VueRouter  from 'vue-router'

import Products   from "./components/Products";
import Product    from "./components/Product";
import Category   from "./components/Category";

Vue.use(VueRouter)

export default function () {
  return new VueRouter({
    //mode: 'history',
    base: __dirname,
    routes: [
      { path: '/home',              name: 'home',           component: Products },
      { path: '/product/new',       name: 'product.new',    component: Product  },
      { path: '/product/:id',       name: 'product.edit',   component: Product  },
      { path: '/category/new',      name: 'category.new',   component: Category },
      { path: '/category/:id',      name: 'category',       component: Products },
      { path: '/category/:id/edit', name: 'category.edit',  component: Category },
    ]
  })
}
