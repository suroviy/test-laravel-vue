/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import 'bulma'

import Vue          from 'vue'
import VueRouter    from 'vue-router'
import VueResource  from 'vue-resource'


import createStore  from './vuex/store'
import createRouter from './router'

import { sync }     from 'vuex-router-sync'

import Menu         from './components/Menu'
import Sidebar      from './components/Sidebar'


Vue.use(VueRouter)
Vue.use(VueResource)

const store   = createStore()
const router  = createRouter()

sync(store, router)

new Vue({
  router,
  store,
  components: {
    Menu: Menu,
    Sidebar: Sidebar,
  },
  template: `
  <div id="app">
    <Menu></Menu>
    <section class="section">
    <div  class="container">
      <div class="columns">
          <div class="column is-one-fifth">
            <Sidebar></Sidebar>
          </div>
          <div class="column">
             <router-view></router-view>
          </div>
      </div>
    </div>
    </section>
    <footer class="footer is-fixed-bottom">
        <div class="container-fluid has-text-centered">
          <p>
            <strong>Test</strong> by Alex P Тестовое задание
          </p>
        </div>
    </footer>
  </div>
  `
}).$mount('#app')
