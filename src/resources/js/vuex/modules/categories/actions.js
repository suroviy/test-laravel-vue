import Vue from 'vue'

let url = '/api/categories'

export const loadFromServer = ({ commit, state }) => {
  commit('SET_IS_LOAD', true)
  Vue.http.get(url).then(response => {
    commit('SET_IS_LOAD', false)
    commit('SET_DATA', response.body.data)
  }, response => {
    console.log(response)
  });
}
