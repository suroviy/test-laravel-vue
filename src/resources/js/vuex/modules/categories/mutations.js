export const SET_DATA = (state, data) => {
  state.data = data
}

export const SET_IS_LOAD = (state, value) => {
  state.isLoad = value
}
