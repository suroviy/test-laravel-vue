export const SET_DATA = (state, data) => {
  state.data = data
}

export const SET_IS_LOAD = (state, value) => {
  state.isLoad = value
}

export const SET_IS_SAVE = (state, value) => {
  state.isSave = value
}

export const SET_ACTIVE_ID = (state, id) => {
  state.activeId = id
}

export const SET_ERRORS = (state, errors) => {
  state.errors = errors
}

export const SET_MOVE = (state, value) => {
  state.move = value
}

export const SET_DATA_NAME = (state, value) => {
  state.data.name = value
}




