import Vue from 'vue'

let url = '/api/categories'

export const editFromServer = ({ commit, state }) => {
  let activeId = state.activeId
  commit('SET_IS_LOAD', true)
  commit('SET_ERRORS', [])
  Vue.http.get(url+'/'+activeId).then(response => {
    if (activeId === state.activeId) {
      commit('SET_IS_LOAD', false)
      commit('SET_DATA', response.body.data)
    }
  }, response => {
    commit('SET_IS_SAVE', false)
    commit('SET_ERRORS', response.body.errors)
  });
}

export const createFromServer = ({ commit, state }) => {
  let activeId = state.activeId
  commit('SET_IS_LOAD', true)
  commit('SET_ERRORS', [])
  Vue.http.get(url+'/create').then(response => {
    if (activeId === state.activeId) {
      commit('SET_IS_LOAD', false)
      commit('SET_DATA', response.body.data)
    }
  }, response => {
    commit('SET_IS_SAVE', false)
    commit('SET_ERRORS', response.body.errors)
  });
}

export const updateToServer = ({ commit, state }) => {
  let activeId = state.activeId
  let data = state.data
  data.move = state.move
  commit('SET_IS_SAVE', true)
  commit('SET_ERRORS', [])
  Vue.http.put(url+'/'+activeId,data).then(response => {
    commit('SET_MOVE', null)
    commit('SET_IS_SAVE', false)
  }, response => {
    commit('SET_IS_SAVE', false)
    commit('SET_ERRORS', response.body.errors)
  });
}

export const storeToServer = ({ commit, state }) => {
  let activeId = state.activeId
  let data = state.data
  commit('SET_IS_SAVE', true)
  commit('SET_ERRORS', [])
  Vue.http.post(url,data).then(response => {
    if (activeId === state.activeId) {
      commit('SET_IS_SAVE', false)
      commit('SET_DATA', response.body.data)
    }
  }, response => {
    commit('SET_IS_SAVE', false)
    commit('SET_ERRORS', response.body.errors)
  });
}

export const destroyToServer = ({ commit, state }) => {
  commit('SET_IS_SAVE', true)
  commit('SET_ERRORS', [])
  Vue.http.delete(url+'/'+state.activeId).then(response => {
     commit('SET_IS_SAVE', false)
     commit('SET_ACTIVE_ID', null)
  }, response => {
    commit('SET_IS_SAVE', false)
    commit('SET_ERRORS', response.body.errors)
  });
}
