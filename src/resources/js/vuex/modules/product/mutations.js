export const SET_DATA = (state, data) => {
  state.data = data
}

export const SET_IS_LOAD = (state, value) => {
  state.isLoad = value
}

export const SET_IS_SAVE = (state, value) => {
  state.isSave = value
}

export const SET_ACTIVE_ID = (state, id) => {
  state.activeId = id
}

export const SET_ERRORS = (state, errors) => {
  state.errors = errors
}

export const SET_DATA_TITLE = (state, value) => {
  state.data.title = value
}

export const SET_DATA_DESCRIPTION = (state, value) => {
  state.data.description = value
}

export const SET_DATA_IMAGE = (state, value) => {
  state.data.image = value
}

export const SET_DATA_IMAGE_URL = (state, value) => {
  state.data.image_url = value
}

export const SET_DATA_CATEGORY_ID = (state, value) => {
  state.data.category_id = value
}

export const SET_ACTIVE_CATEGORY_ID = (state, value) => {
  state.activeCategoryId = value
}

