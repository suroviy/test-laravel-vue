export default  {
  isLoad:false,
  isSave:false,
  activeId: null,
  activeCategoryId: null,
  move: null,
  errors:{},
  data: {
    id:null,
    title: null,
    description: null,
    image: null,
    image_url: null,
    category_id: null
  },
}
