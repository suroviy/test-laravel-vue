export const data             = state => state.data
export const isLoad           = state => state.isLoad
export const isSave           = state => state.isSave
export const activeId         = state => state.activeId
export const activeCategoryId = state => state.activeCategoryId
export const errors           = state => state.errors
