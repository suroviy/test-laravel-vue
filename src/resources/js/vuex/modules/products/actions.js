import Vue from 'vue'

let url = '/api/products'

export const getListFromServer = ({ commit, state }) => {
  let activeCategoryId = state.activeCategoryId
  commit('SET_IS_LOAD', true)
  commit('SET_DATA', [])
  Vue.http.get(url,{params:{
      filter:{
        category_id: activeCategoryId
      }
    }
  }).then(response => {
    if (activeCategoryId === state.activeCategoryId) {
      commit('SET_IS_LOAD', false)
      commit('SET_DATA', response.body.data)
    }
  }, response => {
    console.log(response)
  });

}
