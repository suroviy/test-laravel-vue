export const SET_DATA = (state, data) => {
  state.data = data
}

export const SET_IS_LOAD = (state, value) => {
  state.isLoad = value
}

export const SET_ACTIVE_CATEGORY_ID = (state, id) => {
  state.activeCategoryId = id
}
