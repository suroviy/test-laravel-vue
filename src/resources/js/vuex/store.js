import Vue  from 'vue'
import Vuex from "vuex";

import categories from './modules/categories/store'
import category   from './modules/category/store'
import products   from './modules/products/store'
import product    from './modules/product/store'


Vue.use(Vuex)

export default function () {
  return new Vuex.Store({
    modules: {
      categories: categories,
      category:   category,
      products:   products,
      product:    product,
    }
  })
}
